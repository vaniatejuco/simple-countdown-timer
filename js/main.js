
	let year = 2019;
	let month = 11;
	let day = 26;


	function countdown(){

		const now = new Date();
		let eventDate = new Date(year, month, day);

		let currentTime = now.getTime();
		let eventTime = eventDate.getTime();

		let remTime = eventTime - currentTime;

		let s = Math.floor(remTime / 1000);
		let m = Math.floor(s / 60);
		let h = Math.floor(m / 60);
		let d = Math.floor(h / 24);

		h = h % 24;
		m = m % 60;
		s = s % 60;
		
		h = (h < 10) ? "0" + h : h;
		m = (m < 10) ? "0" + m : m;
		s = (s < 10) ? "0" + s : s;

		document.getElementById("days").innerText = d;
		document.getElementById("days").textContent = d;
		document.getElementById("hours").innerText = h;
		document.getElementById("hours").textContent = h;
		document.getElementById("minutes").innerText = m;
		document.getElementById("minutes").textContent = m;
		document.getElementById("seconds").innerText = s;
		document.getElementById("seconds").textContent = s;

		setTimeout(countdown, 1000);

	}

	countdown();

	


